# README #

这个项目是仿照RTNetworking写的一个网络层解决方案.
[RTNetworking](https://github.com/casatwy/RTNetworking)
### 如何使用 ###

* 继承 WXBaseServiceProvider
```objective-c
@interface CLOTestSeriveProvider : WXBaseServiceProvider<WXServiceProvider,WXServiceParamSourceDelegate,WXServicePacketsValidator>
@end
```
* 加载插件,实现插件的协议
```objective-c
- (WXServiceRequestType)requestType
{
  return WXServiceRequestTypePost;
}
- (NSString*)methodName
{
  return @"Clo_Servlet/api/PaymentCallback";
}
- (NSString*)serviceType
{
  return @"";
}
- (NSDictionary*)paramsForApi:(WXBaseServiceProvider*)provider
{

  NSString* str = @"{\"prcssrId\":\"10652950000\",\"mmbrId\":\"11822\","
                  @"\"seqNo\":\"20141029165512654\",\"trmnlNo\":"
                  @"\"6529500012345\",\"prcssYmd\":20151222,\"saleSum\":1500,"
                  @"\"saleTax\":0,\"chckNo\":\"12345 \",\"prcssTime\":120000}";

    return @{@"prcssrId":@"10652950000",@"mmbrId":@"11822",@"seqNo":@"20141029165512654",@"trmnlNo":@"6529500012345",@"prcssYmd":@20151222,@"saleSum":@1500,@"saleTax":@0,@"chckNo":@"123456",@"prcssTime":@120000};
}
```

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact