//
//  ViewController.m
//  WXProject
//
//  Created by wx on 12/24/15.
//  Copyright © 2015 wx. All rights reserved.
//

#import "ViewController.h"
#import "CLOTestSeriveProvider.h"

@interface ViewController ()<WXServiceCallbackDelegate>

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    CLOTestSeriveProvider *provider = [[CLOTestSeriveProvider alloc]init];
    provider.paramProvider = provider;
    provider.delegate = self;
    provider.validator = provider;
    [provider load];
    // Do any additional setup after loading the view, typically from a nib.
}
- (void)providerCallAPIDidSuccess:(WXBaseServiceProvider*)provider{

}
- (void)providerCallAPIDidFailed:(WXBaseServiceProvider*)provider{

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
