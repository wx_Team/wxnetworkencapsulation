//
//  CLOTestSeriveProvider.m
//  WXProject
//
//  Created by wx on 12/31/15.
//  Copyright © 2015 wx. All rights reserved.
//

#import "CLOTestSeriveProvider.h"

@implementation CLOTestSeriveProvider
- (WXServiceRequestType)requestType
{
  return WXServiceRequestTypePost;
}
- (NSString*)methodName
{
  return @"Clo_Servlet/api/PaymentCallback";
}
- (NSString*)serviceType
{
  return @"";
}
- (NSDictionary*)paramsForApi:(WXBaseServiceProvider*)provider
{

  NSString* str = @"{\"prcssrId\":\"10652950000\",\"mmbrId\":\"11822\","
                  @"\"seqNo\":\"20141029165512654\",\"trmnlNo\":"
                  @"\"6529500012345\",\"prcssYmd\":20151222,\"saleSum\":1500,"
                  @"\"saleTax\":0,\"chckNo\":\"12345 \",\"prcssTime\":120000}";

    return @{@"prcssrId":@"10652950000",@"mmbrId":@"11822",@"seqNo":@"20141029165512654",@"trmnlNo":@"6529500012345",@"prcssYmd":@20151222,@"saleSum":@1500,@"saleTax":@0,@"chckNo":@"123456",@"prcssTime":@120000};
}
- (NSDictionary*)dictionaryWithJsonString:(NSString*)jsonString
{

  if (jsonString == nil) {

    return nil;
  }

  NSData* jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];

  NSError* err;

  NSDictionary* dic =
    [NSJSONSerialization JSONObjectWithData:jsonData

                                    options:NSJSONReadingMutableContainers

                                      error:&err];

  if (err) {

    NSLog(@"json解析失败：%@", err);

    return nil;
  }

  return dic;
}
-(BOOL)provider:(WXBaseServiceProvider *)provider isCorrectWithParamsData:(NSDictionary *)data{
    return YES;
}
-(BOOL)provoder:(WXBaseServiceProvider *)manager isCorrectWithCallBackData:(NSDictionary *)data{
    return YES;
}

@end
