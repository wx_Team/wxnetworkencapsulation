//
//  CLOTestSeriveProvider.h
//  WXProject
//
//  Created by wx on 12/31/15.
//  Copyright © 2015 wx. All rights reserved.
//

#import "WXBaseServiceProvider.h"

@interface CLOTestSeriveProvider : WXBaseServiceProvider<WXServiceProvider,WXServiceParamSourceDelegate,WXServicePacketsValidator>

@end
