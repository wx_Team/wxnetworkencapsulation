//
//  WXBaseServiceProvider.h
//  WXProject
//
//  Created by wx on 12/24/15.
//  Copyright © 2015 wx. All rights reserved.
//

#import "WXServiceProtocols.h"
#import <Foundation/Foundation.h>

@interface WXBaseServiceProvider : NSObject
@property (nonatomic, weak) id<WXServiceParamSourceDelegate> paramProvider;
@property (nonatomic, weak) id<WXServicePacketsValidator> validator;
@property (nonatomic, weak) id<WXServiceCallbackDelegate> delegate;
@property (nonatomic, weak) id<WXServiceProvider> child;
@property (nonatomic, readonly) WXServiceErrorType errorType;

@property (nonatomic, strong,readonly)id fetchedData;

- (NSInteger)load;

- (NSDictionary*)reformParams:(NSDictionary*)params;
@end
