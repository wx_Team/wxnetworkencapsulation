//
//  WXRequestGenerator.h
//  WXProject
//
//  Created by wx on 12/30/15.
//  Copyright © 2015 wx. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WXRequestGenerator : NSObject
+ (instancetype)sharedInstance;

- (NSURLRequest *)generateRestfulGETRequestWithServiceIdentifier:(NSString *)serviceIdentifier requestParams:(NSDictionary *)requestParams methodName:(NSString *)methodName;
- (NSURLRequest *)generateRestfulPOSTRequestWithServiceIdentifier:(NSString *)serviceIdentifier requestParams:(NSDictionary *)requestParams methodName:(NSString *)methodName;

@end
