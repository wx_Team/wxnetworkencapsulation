//
//  WXCommonParamsGenerator.m
//  WXProject
//
//  Created by wx on 12/30/15.
//  Copyright © 2015 wx. All rights reserved.
//

#import "WXCommonParamsGenerator.h"
#import "WXServiceContext.h"
/*
@property (nonatomic, copy) NSString *appName;      //应用名称
@property (nonatomic, copy, readonly) NSString *m;            //设备名称
@property (nonatomic, copy, readonly) NSString *o;            //系统名称
@property (nonatomic, copy, readonly) NSString *v;            //系统版本
@property (nonatomic, copy, readonly) NSString *cv;           //Bundle版本
@property (nonatomic, copy, readonly) NSString *from;
//请求来源，值都是@"mobile"
@property (nonatomic, copy, readonly) NSString *ostype2;      //操作系统类型
@property (nonatomic, copy, readonly) NSString *qtime;        //发送请求的时间
@property (nonatomic, copy, readonly) NSString *macid;
@property (nonatomic, copy, readonly) NSString *uuid;
 */

@implementation WXCommonParamsGenerator
+ (NSDictionary*)commonParamsDictionary
{
  return @{};
}
+ (NSDictionary*)commonParamsDictionaryForLog
{
  WXServiceContext* context = [WXServiceContext sharedInstance];
  return @{
    @"m" : context.m,
    @"o" : context.o,
    @"v" : context.v,
    @"cv" : context.cv,
    @"ostype2" : context.ostype2,
    @"qtime" : context.qtime,
    @"macid" : context.macid,
    @"uuid" : context.uuid
  };
}
@end
