//
//  WXCommonParamsGenerator.h
//  WXProject
//
//  Created by wx on 12/30/15.
//  Copyright © 2015 wx. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WXCommonParamsGenerator : NSObject

+(NSDictionary *)commonParamsDictionary;
+(NSDictionary *)commonParamsDictionaryForLog;

@end
