//
//  WXUDIDGenerator.h
//  WXProject
//
//  Created by wx on 12/30/15.
//  Copyright © 2015 wx. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface WXUDIDGenerator : NSObject
+ (id)sharedInstance;

- (NSString *)UDID;
- (void)saveUDID:(NSString *)udid;
@end
