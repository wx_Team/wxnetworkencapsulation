//
//  WXRequestGenerator.m
//  WXProject
//
//  Created by wx on 12/30/15.
//  Copyright © 2015 wx. All rights reserved.
//

#import "NSDictionary+AXNetworkingMethods.h"
#import "NSURLRequest+AIFNetworkingMethods.h"
#import "WXCommonParamsGenerator.h"
#import "WXRequestGenerator.h"
#import "WXServiceFactory.h"
#import "WXServiceLogger.h"
#import <AFNetworking/AFNetworking.h>
@interface
WXRequestGenerator ()

//@property (nonatomic, strong) AFHTTPRequestSerializer* httpRequestSerializer;

@end
static NSTimeInterval kWXServiceTimeoutSeconds = 20.0f;
@implementation WXRequestGenerator
static WXRequestGenerator* _instance;
#pragma mark -life cycle
+ (instancetype)allocWithZone:(struct _NSZone*)zone
{
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    _instance = [super allocWithZone:zone];
  });
  return _instance;
}

+ (instancetype)sharedInstance
{
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    _instance = [[self alloc] init];
  });
  return _instance;
}

- (id)copyWithZone:(NSZone*)zone
{
  return _instance;
}
#pragma mark - getter and setter
//-(AFHTTPRequestSerializer *)httpRequestSerializer{
//    if (_httpRequestSerializer) {
//        _httpRequestSerializer = [AFJSONRequestSerializer serializer];
//    }
//    return _httpRequestSerializer;
//}
#pragma mark - public method
- (NSURLRequest*)
generateRestfulGETRequestWithServiceIdentifier:(NSString*)serviceIdentifier
                                 requestParams:(NSDictionary*)requestParams
                                    methodName:(NSString*)methodName
{
  NSMutableDictionary* allParams = [NSMutableDictionary
    dictionaryWithDictionary:[WXCommonParamsGenerator commonParamsDictionary]];
  [allParams addEntriesFromDictionary:requestParams];
  WXService* service =
    [[WXServiceFactory sharedInstance] serviceWithIdentifier:serviceIdentifier];
  NSString* urlString;
  if (allParams && [allParams isEqual:[NSNull null]]) {
    urlString =
      [NSString stringWithFormat:@"%@/%@?%@", service.apiBaseUrl, methodName,
                                 [allParams AIF_urlParamsStringSignature:NO]];
  } else {
    urlString =
      [NSString stringWithFormat:@"%@/%@", service.apiBaseUrl, methodName];
  }

  NSMutableURLRequest* request =
    [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                            cachePolicy:NSURLRequestUseProtocolCachePolicy
                        timeoutInterval:kWXServiceTimeoutSeconds];
  NSDictionary* restfulHeader = [self commonRESTHeadersWithService:service];
  request.HTTPMethod = @"GET";
  [restfulHeader enumerateKeysAndObjectsUsingBlock:^(
                   id _Nonnull key, id _Nonnull obj, BOOL* _Nonnull stop) {
    [request setValue:obj forHTTPHeaderField:key];
  }];
  request.requestParams = requestParams;
  [WXServiceLogger logDebugInfoWithRequest:request
                                   apiName:methodName
                                   service:service
                             requestParams:requestParams
                                httpMethod:@"GET"];
  return request;
}
- (NSURLRequest*)
generateRestfulPOSTRequestWithServiceIdentifier:(NSString*)serviceIdentifier
                                  requestParams:(NSDictionary*)requestParams
                                     methodName:(NSString*)methodName
{
  WXService* service =
    [[WXServiceFactory sharedInstance] serviceWithIdentifier:serviceIdentifier];

  NSMutableDictionary* allParams = [NSMutableDictionary
    dictionaryWithDictionary:[WXCommonParamsGenerator commonParamsDictionary]];
  [allParams addEntriesFromDictionary:requestParams];
  NSString* urlString =
    [NSString stringWithFormat:@"%@/%@", service.apiBaseUrl, methodName];
  NSDictionary* restfulHeader = [self commonRESTHeadersWithService:service];
  NSMutableURLRequest* request =
    [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                            cachePolicy:NSURLRequestUseProtocolCachePolicy
                        timeoutInterval:kWXServiceTimeoutSeconds];
  request.HTTPMethod = @"POST";
  [restfulHeader enumerateKeysAndObjectsUsingBlock:^(
                   id _Nonnull key, id _Nonnull obj, BOOL* _Nonnull stop) {
    [request setValue:obj forHTTPHeaderField:key];
  }];
  request.HTTPBody =
    [NSJSONSerialization dataWithJSONObject:allParams
                                    options:NSJSONWritingPrettyPrinted
                                      error:NULL];
  request.requestParams = requestParams;
  [WXServiceLogger logDebugInfoWithRequest:request
                                   apiName:methodName
                                   service:service
                             requestParams:requestParams
                                httpMethod:@"POST"];
  return request;
}

#pragma mark - private method
- (NSDictionary*)commonRESTHeadersWithService:(WXService*)service
{
  NSMutableDictionary* headerDic = [NSMutableDictionary dictionary];
  [headerDic setValue:@"application/json" forKey:@"Accept"];
  [headerDic setValue:@"application/json" forKey:@"Content-Type"];
  return headerDic;
}

@end
