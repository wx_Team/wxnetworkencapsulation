//
//  WXBaseServiceProvider.m
//  WXProject
//
//  Created by wx on 12/24/15.
//  Copyright © 2015 wx. All rights reserved.
//

#import "WXApiProxy.h"
#import "WXBaseServiceProvider.h"
#import "WXServiceContext.h"
@interface
WXBaseServiceProvider ()
@property (nonatomic, strong) NSMutableArray* requestIdList;
@property (nonatomic, readwrite) WXServiceErrorType errorType;
@property (nonatomic, strong, readwrite) id fetchedData;
@end

@implementation WXBaseServiceProvider
#pragma mark - life cycle
- (instancetype)init
{
  self = [super init];
  if (self) {
    _delegate = nil;
    _paramProvider = nil;
    _validator = nil;
    if ([self conformsToProtocol:@protocol(WXServiceProvider)]) {
      self.child = (id<WXServiceProvider>)self;
    }
  }
  return self;
}
- (NSInteger)load
{
  NSDictionary* params = [self.paramProvider paramsForApi:self];
  NSInteger requestId = [self loadDataWithParams:params];
  return requestId;
}
- (NSInteger)loadDataWithParams:(NSDictionary*)params
{
  NSInteger requestId = 0;
  NSDictionary* apiParams = [self reformParams:params];
  if (!self.validator ||
      [self.validator provider:self isCorrectWithParamsData:params]) {
    if ([self isReachable]) {

      switch (self.child.requestType) {
        case WXServiceRequestTypeGet: {

          [[WXApiProxy sharedInstance] callRestfulGETWithParams:apiParams
            serviceIdentifier:self.child.serviceType
            methodName:self.child.methodName
            success:^(WXURLResponse* response) {
              [self successedOnCallingAPI:response];
            }
            fail:^(WXURLResponse* response) {
              [self failedOnCallingAPI:response
                         withErrorType:WXServiceErrorTypeDefault];
            }];
          [self.requestIdList addObject:@(requestId)];

        }

        break;
        case WXServiceRequestTypePost: {
          [[WXApiProxy sharedInstance] callRestfulPOSTWithParams:apiParams
            serviceIdentifier:self.child.serviceType
            methodName:self.child.methodName
            success:^(WXURLResponse* response) {
              [self successedOnCallingAPI:response];
            }
            fail:^(WXURLResponse* response) {
              [self failedOnCallingAPI:response
                         withErrorType:WXServiceErrorTypeDefault];
            }];
          [self.requestIdList addObject:@(requestId)];

        }

        break;

        default:
          break;
      }
      return requestId;
    } else {
      [self failedOnCallingAPI:nil withErrorType:WXServiceErrorTypeNoNetWork];
      return requestId;
    }

  } else {
    [self failedOnCallingAPI:nil withErrorType:WXServiceErrorTypeParamsError];
    return requestId;
  }
  return requestId;
}

- (void)successedOnCallingAPI:(WXURLResponse*)response
{
  [self removeRequestIdWithRequestID:response.requestId];
  if (!self.validator || [self.validator provoder:self
                           isCorrectWithCallBackData:response.content]) {
    [self.delegate providerCallAPIDidSuccess:self];
  } else {
    [self failedOnCallingAPI:response
               withErrorType:WXServiceErrorTypeNoContent];
  }
}

- (void)failedOnCallingAPI:(WXURLResponse*)response
             withErrorType:(WXServiceErrorType)errorType
{
  self.errorType = errorType;
  [self removeRequestIdWithRequestID:response.requestId];
  [self.delegate providerCallAPIDidFailed:self];
}

- (NSDictionary*)reformParams:(NSDictionary*)params
{
  return params;
}

- (BOOL)isReachable
{
  BOOL isReachability = [WXServiceContext sharedInstance].isReachable;
  return isReachability;
}
#pragma mark - private methods
- (void)removeRequestIdWithRequestID:(NSInteger)requestId
{
  NSNumber* requestIDToRemove = nil;
  for (NSNumber* storedRequestId in self.requestIdList) {
    if ([storedRequestId integerValue] == requestId) {
      requestIDToRemove = storedRequestId;
    }
  }
  if (requestIDToRemove) {
    [self.requestIdList removeObject:requestIDToRemove];
  }
}
- (BOOL)checkJson:(id)json withValidator:(id)validatorJson
{
  if ([json isKindOfClass:[NSDictionary class]] &&
      [validatorJson isKindOfClass:[NSDictionary class]]) {
    NSDictionary* dict = json;
    NSDictionary* validator = validatorJson;
    BOOL result = YES;
    NSEnumerator* enumerator = [validator keyEnumerator];
    NSString* key;
    while ((key = [enumerator nextObject]) != nil) {
      id value = dict[key];
      id format = validator[key];
      if ([value isKindOfClass:[NSDictionary class]] ||
          [value isKindOfClass:[NSArray class]]) {
        result = [self checkJson:value withValidator:format];
        if (!result) {
          break;
        }
      } else {
        if ([value isKindOfClass:format] == NO &&
            [value isKindOfClass:[NSNull class]] == NO) {
          result = NO;
          break;
        }
      }
    }
    return result;
  } else if ([json isKindOfClass:[NSArray class]] &&
             [validatorJson isKindOfClass:[NSArray class]]) {
    NSArray* validatorArray = (NSArray*)validatorJson;
    if (validatorArray.count > 0) {
      NSArray* array = json;
      NSDictionary* validator = validatorJson[0];
      for (id item in array) {
        BOOL result = [self checkJson:item withValidator:validator];
        if (!result) {
          return NO;
        }
      }
    }
    return YES;
  } else if ([json isKindOfClass:validatorJson]) {
    return YES;
  } else {
    return NO;
  }
}

@end
