//
//  WXURLResponse.h
//  WXProject
//
//  Created by wx on 12/29/15.
//  Copyright © 2015 wx. All rights reserved.
//

#import "WXServiceProtocols.h"
#import <Foundation/Foundation.h>

@interface WXURLResponse : NSObject
@property (nonatomic, assign, readonly) WXURLResponseStatus status;
@property (nonatomic, copy, readonly) NSString* contentString;
@property (nonatomic, copy, readonly) id content;
@property (nonatomic, readonly) NSInteger requestId;
@property (nonatomic, copy, readonly) NSURLRequest* request;
@property (nonatomic, copy) NSDictionary* requestParams;

- (instancetype)initWithResponseString:(NSString*)responseString
                       responseContent:(id)responseContent
                             requestId:(NSNumber*)requestId
                               request:(NSURLRequest*)request
                                status:(WXURLResponseStatus)status;
- (instancetype)initWithResponseString:(NSString*)responseString
                             responseContent:(id)responseContent
                             requestId:(NSNumber*)requestId
                               request:(NSURLRequest*)request
                                 error:(NSError*)error;
@end
