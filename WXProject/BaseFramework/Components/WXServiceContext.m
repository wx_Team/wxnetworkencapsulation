//
//  WXServiceContext.m
//  WXProject
//
//  Created by wx on 12/28/15.
//  Copyright © 2015 wx. All rights reserved.
//

#import "WXServiceContext.h"
#import <AFNetworking/AFNetworkReachabilityManager.h>
#import "UIDevice+IdentifierAddition.h"
#import "NSObject+AXNetworkingMethods.h"
@interface WXServiceContext()
@property (nonatomic, strong) UIDevice *device;
@property (nonatomic, copy, readwrite) NSString *m;            //设备名称
@property (nonatomic, copy, readwrite) NSString *o;            //系统名称
@property (nonatomic, copy, readwrite) NSString *v;            //系统版本
@property (nonatomic, copy, readwrite) NSString *cv;           //Bundle版本
@property (nonatomic, copy, readwrite) NSString *ostype2;      //操作系统类型
@property (nonatomic, copy, readwrite) NSString *qtime;        //发送请求的时间
@property (nonatomic, copy, readwrite) NSString *macid;
@property (nonatomic, copy, readwrite) NSString *uuid;

@end


@implementation WXServiceContext

#pragma mark - life cycle

static WXServiceContext *_instance;

+ (instancetype)allocWithZone:(struct _NSZone *)zone
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [super allocWithZone:zone];
    });
    return _instance;
}

+ (instancetype)sharedInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [[self alloc] init];
    });
    return _instance;
}

- (id)copyWithZone:(NSZone *)zone
{
    return _instance;
}

#pragma mark - getters and setters
- (UIDevice *)device
{
    if (_device == nil) {
        _device = [UIDevice currentDevice];
    }
    return _device;
}

- (NSString *)m
{
    if (_m == nil) {
        _m = [[self.device.model stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] AIF_defaultValue:@""];
    }
    return _m;
}

- (NSString *)o
{
    if (_o == nil) {
        _o = [[self.device.systemName stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] AIF_defaultValue:@""];
    }
    return _o;
}
- (NSString *)v
{
    if (_v == nil) {
        _v = [self.device systemVersion];
    }
    return _v;
}
- (NSString *)cv
{
    if (_cv == nil) {
        _cv = [[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"] AIF_defaultValue:@""];
    }
    return _cv;
}
- (NSString *)macid
{
    if (_macid == nil) {
        _macid = [[self.device AIF_macaddressMD5] AIF_defaultValue:@""];
    }
    return _macid;
}
- (NSString *)qtime
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:NSLocalizedString(@"yyyyMMddHHmmss", nil)];
    return [formatter stringFromDate:[NSDate date]];
}
- (NSString *)ostype2
{
    if (_ostype2 == nil) {
        _ostype2 = [self.device.AIF_ostype AIF_defaultValue:@""];
    }
    return _ostype2;
}
-(NSString *)uuid{
    if (_uuid == nil) {
        _uuid = [[self.device AIF_uuid] AIF_defaultValue:@""];
    }
    return _uuid;
}




-(BOOL)isReachable{
    if ([AFNetworkReachabilityManager sharedManager].networkReachabilityStatus == AFNetworkReachabilityStatusUnknown) {
        return YES;
    } else {
        return [[AFNetworkReachabilityManager sharedManager] isReachable];
    }

}

@end
