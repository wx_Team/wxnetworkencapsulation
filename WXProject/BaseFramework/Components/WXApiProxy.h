//
//  WXApiProxy.h
//  WXProject
//
//  Created by wx on 12/29/15.
//  Copyright © 2015 wx. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WXURLResponse.h"

typedef void(^WXCallback)(WXURLResponse *response);
@interface WXApiProxy : NSObject
+(instancetype)sharedInstance;

- (NSInteger)callRestfulGETWithParams:(NSDictionary *)params serviceIdentifier:(NSString *)servieIdentifier methodName:(NSString *)methodName success:(WXCallback)success fail:(WXCallback)fail;
- (NSInteger)callRestfulPOSTWithParams:(NSDictionary *)params serviceIdentifier:(NSString *)servieIdentifier methodName:(NSString *)methodName success:(WXCallback)success fail:(WXCallback)fail;

- (void)cancelRequestWithRequestID:(NSNumber *)requestID;
- (void)cancelRequestWithRequestIDList:(NSArray *)requestIDList;

@end
