//
//  WXServiceLogger.h
//  WXProject
//
//  Created by wx on 12/24/15.
//  Copyright © 2015 wx. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WXService.h"

@interface WXServiceLogger : NSObject
+ (void)logDebugInfoWithRequest:(NSURLRequest *)request apiName:(NSString *)apiName service:(WXService *)service requestParams:(id)requestParams httpMethod:(NSString *)httpMethod;
+ (void)logDebugInfoWithResponse:(NSHTTPURLResponse *)response resposeString:(NSString *)responseString request:(NSURLRequest *)request error:(NSError *)error;

@end
