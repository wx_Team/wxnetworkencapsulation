//
//  WXApiProxy.m
//  WXProject
//
//  Created by wx on 12/29/15.
//  Copyright © 2015 wx. All rights reserved.
//

#import "WXApiProxy.h"
#import "WXRequestGenerator.h"
#import "WXServiceLogger.h"
#import <AFNetworking/AFNetworking.h>
@interface
WXApiProxy ()
@property (nonatomic, strong) NSMutableDictionary* dispatchTable;
@property (nonatomic, strong) NSNumber* recordedRequestId;
@property (nonatomic, strong) AFURLSessionManager* sessionManager;
@end

@implementation WXApiProxy
#pragma mark - lifecycle
static WXApiProxy* _instance;

+ (instancetype)allocWithZone:(struct _NSZone*)zone
{
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    _instance = [super allocWithZone:zone];
  });
  return _instance;
}

+ (instancetype)sharedInstance
{
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    _instance = [[self alloc] init];
  });
  return _instance;
}

- (id)copyWithZone:(NSZone*)zone
{
  return _instance;
}
- (AFURLSessionManager*)sessionManager
{
  if (!_sessionManager) {
    NSURLSessionConfiguration* configuration =
      [NSURLSessionConfiguration defaultSessionConfiguration];
    AFSecurityPolicy* policy =
      [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    policy.allowInvalidCertificates = YES;
    policy.validatesDomainName = NO;
    _sessionManager =
      [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    _sessionManager.securityPolicy = policy;
  }
  return _sessionManager;
}

#pragma mark - getters and setters
- (NSMutableDictionary*)dispatchTable
{
  if (_dispatchTable == nil) {
    _dispatchTable = [[NSMutableDictionary alloc] init];
  }
  return _dispatchTable;
}

#pragma mark - public method
- (NSInteger)callRestfulGETWithParams:(NSDictionary*)params
                    serviceIdentifier:(NSString*)servieIdentifier
                           methodName:(NSString*)methodName
                              success:(WXCallback)success
                                 fail:(WXCallback)fail
{
  NSURLRequest* request = [[WXRequestGenerator sharedInstance]
    generateRestfulGETRequestWithServiceIdentifier:servieIdentifier
                                     requestParams:params
                                        methodName:methodName];
  NSNumber* requestId =
    [self callApiWithRequest:request success:success fail:fail];
  return [requestId integerValue];
}
- (NSInteger)callRestfulPOSTWithParams:(NSDictionary*)params
                     serviceIdentifier:(NSString*)servieIdentifier
                            methodName:(NSString*)methodName
                               success:(WXCallback)success
                                  fail:(WXCallback)fail
{
  NSURLRequest* request = [[WXRequestGenerator sharedInstance]
    generateRestfulPOSTRequestWithServiceIdentifier:servieIdentifier
                                      requestParams:params
                                         methodName:methodName];
  NSNumber* requestId =
    [self callApiWithRequest:request success:success fail:fail];
  return [requestId integerValue];
}
- (NSNumber*)callApiWithRequest:(NSURLRequest*)request
                        success:(WXCallback)success
                           fail:(WXCallback)fail
{
  NSNumber* requestId = [self generateRequestId];

  NSURLSessionTask* task = [self.sessionManager
    dataTaskWithRequest:request
      completionHandler:^(NSURLResponse* _Nonnull response,
                          id _Nullable responseObject,
                          NSError* _Nullable error) {

        NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
        NSString* responseString =
          [NSString stringWithFormat:@"%@", responseObject];
        NSURLSessionTask* storedTask = self.dispatchTable[requestId];
        if (storedTask == nil) {
          return;
        } else {
          [self.dispatchTable removeObjectForKey:requestId];
        }
        if (error) {
          [WXServiceLogger logDebugInfoWithResponse:httpResponse
                                      resposeString:responseString
                                            request:request
                                              error:error];
          WXURLResponse* URLResponse =
            [[WXURLResponse alloc] initWithResponseString:responseString
                                          responseContent:responseObject
                                                requestId:requestId
                                                  request:request
                                                    error:error];
          fail ? fail(URLResponse) : nil;
        } else {

          [WXServiceLogger logDebugInfoWithResponse:httpResponse
                                      resposeString:responseString
                                            request:request
                                              error:error];
          WXURLResponse* URLResponse = [[WXURLResponse alloc]
            initWithResponseString:responseString
                   responseContent:responseObject
                         requestId:requestId
                           request:request
                            status:WXURLResponseStatusSuccess];
          success ? success(URLResponse) : nil;
        }
      }];
  self.dispatchTable[requestId] = task;
  [task resume];
  return requestId;
}
- (void)cancelRequestWithRequestID:(NSNumber*)requestID
{
  NSURLSessionTask* task = self.dispatchTable[requestID];
  [task cancel];
  [self.dispatchTable removeObjectForKey:requestID];
}
- (void)cancelRequestWithRequestIDList:(NSArray*)requestIDList
{
  for (NSNumber* number in self.dispatchTable) {
    [self cancelRequestWithRequestID:number];
  }
}
#pragma mark - private method
- (NSNumber*)generateRequestId
{
  if (_recordedRequestId == nil) {
    _recordedRequestId = @(1);
  } else {
    if ([_recordedRequestId integerValue] == NSIntegerMax) {
      _recordedRequestId = @(1);
    } else {
      _recordedRequestId = @([_recordedRequestId integerValue] + 1);
    }
  }
  return _recordedRequestId;
}

@end
