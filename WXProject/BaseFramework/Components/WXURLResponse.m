//
//  WXURLResponse.m
//  WXProject
//
//  Created by wx on 12/29/15.
//  Copyright © 2015 wx. All rights reserved.
//

#import "NSObject+AXNetworkingMethods.h"
#import "NSURLRequest+AIFNetworkingMethods.h"
#import "WXURLResponse.h"
@interface
WXURLResponse ()
@property (nonatomic, assign, readwrite) WXURLResponseStatus status;
@property (nonatomic, copy, readwrite) NSString* contentString;
@property (nonatomic, copy, readwrite) id content;
@property (nonatomic, copy, readwrite) NSURLRequest* request;
@property (nonatomic, assign, readwrite) NSInteger requestId;
@end
@implementation WXURLResponse
#pragma mark - life cycle
- (instancetype)initWithResponseString:(NSString*)responseString
                       responseContent:(id)responseContent
                             requestId:(NSNumber*)requestId
                               request:(NSURLRequest*)request
                                status:(WXURLResponseStatus)status
{
  self = [super init];
  if (self) {
    self.contentString = responseString;

    self.content = responseContent;
    self.status = status;
    self.requestId = [requestId integerValue];
    self.request = request;
    self.requestParams = request.requestParams;
  }
  return self;
}

- (instancetype)initWithResponseString:(NSString*)responseString
                       responseContent:(id)responseContent
                             requestId:(NSNumber*)requestId
                               request:(NSURLRequest*)request
                                 error:(NSError*)error
{
  self = [super init];
  if (self) {
    self.contentString = [responseString AIF_defaultValue:@""];
    self.status = [self responseStatusWithError:error];
    self.requestId = [requestId integerValue];
    self.request = request;
    self.requestParams = request.requestParams;
    self.content = responseContent;
  }
  return self;
}
#pragma mark - private methods
- (WXURLResponseStatus)responseStatusWithError:(NSError*)error
{
  if (error) {
    WXURLResponseStatus result = WXURLResponseStatusErrorNoNetwork;

    // 除了超时以外，所有错误都当成是无网络
    if (error.code == NSURLErrorTimedOut) {
      result = WXURLResponseStatusErrorNoNetwork;
    }
    return result;
  } else {
    return WXURLResponseStatusSuccess;
  }
}

@end
