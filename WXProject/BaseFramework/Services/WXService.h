//
//  WXService.h
//  WXProject
//
//  Created by wx on 12/24/15.
//  Copyright © 2015 wx. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WXService : NSObject

@property (nonatomic, readonly) BOOL isOnline;
@property (nonatomic, strong, readonly) NSString *apiBaseUrl;
@property (nonatomic, strong,readonly) NSString* onlineApiBaseUrl;
@property (nonatomic, strong,readonly) NSString* offlineApiBaseUrl;


@end
