//
//  WXServiceFactory.h
//  WXProject
//
//  Created by wx on 12/24/15.
//  Copyright © 2015 wx. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WXService.h"

@interface WXServiceFactory : NSObject
+(instancetype)sharedInstance;
-(WXService *)serviceWithIdentifier:(NSString *)identifier;
@end
