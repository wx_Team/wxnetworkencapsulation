//
//  WXServiceFactory.m
//  WXProject
//
//  Created by wx on 12/24/15.
//  Copyright © 2015 wx. All rights reserved.
//

#import "WXServiceFactory.h"
#import "WXService.h"
#import "CLOTestService.h"
@interface WXServiceFactory()

@property (nonatomic,strong)NSMutableDictionary *serviceStorage;

@end

@implementation WXServiceFactory
#pragma mark - getters and setters
-(NSMutableDictionary *)serviceStorage{
    if (_serviceStorage == nil) {
        _serviceStorage = [[NSMutableDictionary alloc]init];
    }
    return _serviceStorage;
}

#pragma mark - life cycle
static WXServiceFactory *_instance;

+ (instancetype)allocWithZone:(struct _NSZone *)zone
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [super allocWithZone:zone];
    });
    return _instance;
}

+ (instancetype)sharedInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [[self alloc] init];
    });
    return _instance;
}

- (id)copyWithZone:(NSZone *)zone
{
    return _instance;
}
#pragma mark - public methods
-(WXService *)serviceWithIdentifier:(NSString *)identifier{
    if (self.serviceStorage[identifier] == nil) {
        self.serviceStorage[identifier] = [self newServiceWithIdentifier:identifier];
    }
    return self.serviceStorage[identifier];
}
#pragma mark - private methods
-(WXService *)newServiceWithIdentifier:(NSString *)identifier{
    return [[CLOTestService alloc]init];
}

@end
