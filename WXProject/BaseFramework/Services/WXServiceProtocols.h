//
//  WXProtocols.h
//  WXProject
//
//  Created by wx on 12/24/15.
//  Copyright © 2015 wx. All rights reserved.
//

#import <Foundation/Foundation.h>
@class WXBaseServiceProvider;
typedef NS_ENUM(NSUInteger, WXServiceRequestType) {
  WXServiceRequestTypeGet,
  WXServiceRequestTypePost
};
typedef NS_ENUM(NSInteger, WXServiceErrorType) {
  WXServiceErrorTypeDefault = 0,
  WXServiceErrorTypeSuccess = 1,
  WXServiceErrorTypeNoContent = 2,
  WXServiceErrorTypeParamsError = 3,
  WXServiceErrorTypeTimeout = 4,
  WXServiceErrorTypeNoNetWork = 5
};
typedef NS_ENUM(NSUInteger, WXURLResponseStatus) {
  WXURLResponseStatusSuccess, //
  WXURLResponseStatusErrorTimeout,
  WXURLResponseStatusErrorNoNetwork // 默认除了超时以外的错误都是无网络错误。
};
@protocol WXServiceProvider<NSObject>
- (NSString*)methodName;
- (NSString*)serviceType;
- (WXServiceRequestType)requestType;
@end

@protocol WXServiceParamSourceDelegate<NSObject>
- (NSDictionary*)paramsForApi:(WXBaseServiceProvider*)provider;
@end

@protocol WXServicePacketsValidator<NSObject>
- (BOOL)provider:(WXBaseServiceProvider*)provider
  isCorrectWithParamsData:(NSDictionary*)data;
- (BOOL)provoder:(WXBaseServiceProvider*)manager
  isCorrectWithCallBackData:(NSDictionary*)data;
@end

@protocol WXServiceCallbackDelegate<NSObject>
- (void)providerCallAPIDidSuccess:(WXBaseServiceProvider*)provider;
- (void)providerCallAPIDidFailed:(WXBaseServiceProvider*)provider;
@end

@protocol WXServiceCallbackDataReformer<NSObject>
- (id)provider:(WXBaseServiceProvider*)provider reformData:(NSDictionary*)data;
@end
