//
//  WXService.m
//  WXProject
//
//  Created by wx on 12/24/15.
//  Copyright © 2015 wx. All rights reserved.
//

#import "WXService.h"
@interface
WXService ()
@property (nonatomic, strong,readwrite) NSString* onlineApiBaseUrl;
@property (nonatomic, strong,readwrite) NSString* offlineApiBaseUrl;
@end

@implementation WXService
- (NSString*)apiBaseUrl
{
  return self.isOnline ? self.onlineApiBaseUrl : self.offlineApiBaseUrl;
}
-(NSString *)onlineApiBaseUrl{
    [self doesNotRecognizeSelector:_cmd];
    return nil;
}
-(NSString *)offlineApiBaseUrl{
    [self doesNotRecognizeSelector:_cmd];
    return nil;
}
@end
